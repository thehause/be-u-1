json.extract! service, :id, :title, :description, :require, :price, :user_id, :created_at, :updated_at
json.url service_url(service, format: :json)